# Atividade de introdução ao React

Criar um App que exiba a frase **Hello World!** em 10 formas diferentes. </br>

Além disso, **exiba a data atual em cada uma das formas**, para isso será necessário o [JavaScript Date Objects](https://www.w3schools.com/js/js_dates.asp).</br>

Seja criativo, por exemplo, use letras maiúsculas, minúsculas, de trás pra frente e etc. </br>

Usem o primeiro desafio como referencia e faça as formas difrentes sem utilizar CSS ou **simplesmente escrever** Hello World de trás pra frente, utilizem **Javascript!** </br>

* No terminal e execute o comando **create-react-app introducao-ao-react**, isso criará as pastas e arquivos bases do React; </br>

* Com as pastas base criadas, reescreva o arquivo App.js para que tenha até 10 textos "Hello World" diferentes. Poderá ser utilizado CSS ou métodos Javascript que alterem o texto; </br>

* Para rodar não se esqueça de usar **npm run start** para que o App continue sendo buildado sempre que atualizar os arquivos; </br>

* Faça **commits** para cada forma diferente de exibição do texto; </br>

* O nome do Merge Request deve ser o seu nome completo. </br>

Para se destacar na execução: </br>

Utilize **props** e apenas um componente para exibir o Hello World. </br>

* Crie uma branch **seu-nome/introducao-ao-react** no seu repositório e faça o MR de entrega a partir dela; </br>

* **Personalize** a página com CSS;
