import React from 'react';
import './index.css';

function Message(props) {
    return (
        <div style={props.style}> 
            <div>{props.name}</div>
            <div>{new Date().toString()}</div>
        </div>
    );
}

export default Message;