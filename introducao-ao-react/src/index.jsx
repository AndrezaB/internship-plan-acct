import React from 'react';
import ReactDOM from 'react-dom/client';
import Message from './components/Message';
import './index.css';
import logo from './logo.svg';
  
function App() {
  const hello = 'hello world!';

  return (
    <div id='container'>
      <div className='header'>
        <h1>Introdução ao React</h1>
        <img src={logo} className="App-logo" alt="logo" />
      </div>
      <div className="content">
        <div className='display'>
          <Message name={hello.toUpperCase()} style={{color:'#345475'}}/>
          <Message name={hello.toLowerCase()} style={{color: '#104275'}}/>
          <Message name={hello.replaceAll('o', '0')} style={{color:'#1B6EC2'}}/>
          <Message name={capitalizeFirstLetter(hello)} style={{color:'#228CF5'}}/>
          <Message name={capitalizeChars(hello)} style={{color:'#6CB1F7'}}/>
        </div>
        <div className='display'>
          <Message name={hello.split('').reverse().join('')} style={{color:'#2F4275'}}/>
          <Message name={hello.split('').join('.')} style={{color:'#0C2875'}}/>
          <Message name={hello.split('').reverse().join('.')} style={{color:'#1343C2'}}/>
          <Message name={hello.split('').reverse().join(' ')} style={{color:'#1853F5'}}/>
          <Message name={hello.split('').join('').replaceAll('l', 'L')} style={{color:'#638AF7'}}/>
        </div>
      </div>
    </div>
  );

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function capitalizeChars(string) {
    let arr = string.split(' ');

    return capitalizeFirstLetter(arr[0]) + ' ' + capitalizeFirstLetter(arr[1]);
  }
}

const root = ReactDOM.createRoot(document.getElementById('root'));
const element = <App />;
root.render(element);