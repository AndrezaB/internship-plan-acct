# Plano de Estágio da ACCT
#### Criar um repositório no Gitlab ou github para  armazenar exercícios desenvolvidos.

## Módulo 01
### Tópico 4: Javascript.

> ## Desafio

 Neste desafio vocês deverão criar uma calculadora e ela deve pelo menos fazer uma **soma e subtração.**

Obrigatório:

* Organize em funções diferentes para cada ação diferente, por exemplo uma função para o click nos botões numéricos e outra função para os botões de operação.
* Crie a estrutura HTML padrão, **não faça os elementos dinâmicos.** (como no desafio anterior)
* Personalize a calculadora com CSS, deixe com a cara da calculadora de seu Sistema Operacional.

Para se destacar na execução:

* Faça também as operações de multiplicação e divisão
* Faça com que possa ser feito vários cálculos na mesma digitação, ex. Digitado: 1 + 2 * 2, Resultado: 5
* Crie uma branch seu-nome/basico-dos-componentes no seu repositório.
* Se preocupe com a visualização no celular. [Media Queries](https://www.w3schools.com/css/css_rwd_mediaqueries.asp).