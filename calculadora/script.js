'use strict';

const display = document.getElementsByClassName('current-operation');
const numbers = document.querySelectorAll('[id*=number]');
const operations = document.querySelectorAll('.operation');

let displayValue = '0';

display[0].textContent = displayValue;

const inserirNumero = (event) => {

    if(displayValue === '0') {
        displayValue = event.target.textContent;
    } else {
        displayValue = displayValue + event.target.textContent;
    }

    display[0].textContent = displayValue;
}

const inserirOperacao = (event) => {
    let item = event.target.textContent;

    if(validateInsert(display[0].textContent, item)) {
        if(displayValue === '0') {
            displayValue = item;
        } else {
            displayValue = displayValue + ` ${event.target.textContent} `;
        }
    
        display[0].textContent = displayValue;
    }
}

numbers.forEach(number => {
    number.addEventListener('click', inserirNumero);
});

operations.forEach(operation => {
    operation.addEventListener('click', inserirOperacao);
});

function cleanDisplay() {
    displayValue = '0';
    display[0].textContent = displayValue;
}

function cleanChar() {
    displayValue = display[0].textContent.slice(0, -1);
    
    if(displayValue.length == 0) {
        displayValue = '0'
    }
    
    display[0].textContent = displayValue;
}

function validateInsert(expression, currentItem) {
    expression = expression.trim();

    let lastChar = expression.slice(-1);

    if((lastChar === 'x' || lastChar === '/' || lastChar === '+' || lastChar === '-') 
    && (currentItem === 'x' || currentItem === '/' || currentItem === '+' || currentItem === '-')) {
        return false;
    } else {
        return true;
    }
}

function calculate() {
    let textExpression = (document.querySelector('.current-operation').innerText).toString().replaceAll(' ', '').replaceAll('x', '*');

    document.querySelector('.previous-operation').textContent = eval(textExpression);
}